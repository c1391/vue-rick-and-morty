import VueRouter from "vue-router";
import Vue from "vue";
import ContactList from '@/components/ContactList'
import ContactDetail from '@/components/ContactDetail'

Vue.use(VueRouter)

const routes = [
    {
        path: '/contacts',
        alias: '/',
        component: ContactList
    },
    {
        path: '/contacts/:id',
        component: ContactDetail
    }
]


export default new VueRouter({
    routes
})












