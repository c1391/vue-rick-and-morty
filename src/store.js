import Vuex from 'vuex'
import Vue from "vue";
import {CharacterAPI} from "@/api/characterAPI";

Vue.use(Vuex)

export default new Vuex.Store({
    state: { // Global State
        loadingCharacters: true,
        charactersError: '',
        characters: [],
        searchText: '',
    },
    getters: {
        numberOfCharacters: state => {
            return state.characters.length
        },
        charactersByFilter: state => {
            return state.characters.filter(c => c.name.toLowerCase().includes(state.searchText.toLowerCase()))
        }
    },
    mutations: {
        setLoadingCharacters: (state, payload) => {
            state.loadingCharacters = payload
        },
        setCharactersError: (state, payload) => {
            state.charactersError = payload
        },
        setCharacters: (state, payload) => {
            state.characters = payload
        },
        deleteCharacter: (state, payload) => { // id
            state.characters = state.characters.filter(c => c.id !== payload)
        },
        setSearchFilterText: (state, payload) => {
            state.searchText = payload
        }
    },
    actions: {
        async fetchCharacters({commit, state}) {
            if (state.characters.length !== 0)
                return new Promise(resove => resove())

            const [error, characters] = await CharacterAPI.get()
            commit('setLoadingCharacters', false)
            commit('setCharactersError', error)
            commit('setCharacters', characters)
        },
        async deleteCharacter({ commit }, id) {
            return new Promise((resolve) => {
                commit('deleteCharacter', id)
                resolve()
            })
        }
    }
})









