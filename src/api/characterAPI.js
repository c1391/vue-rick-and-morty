export const CharacterAPI = {

    async get() {
        try {
            const {results} = await fetch('https://rickandmortyapi.com/api/character')
                .then(r => r.json())
            return [null, results]
        } catch (e) {
            return [e.message, []]
        }
    },

    async getById(id) {
        try {
            const response = await fetch(`https://rickandmortyapi.com/api/character/${id}`)
                .then(r => r.json())
            return [null, response]
        } catch (e) {
            return [e.message, {}]
        }
    }
}
